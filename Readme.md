# Démo Explorer

## A propos
* Démo de l'explorer.js
  
* voir le script [ici](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/explorer.js) et sur [npmjs](https://www.npmjs.com/package/elan-data-explorer.js)

## installation
```
npm install elan-data-explorer.js
```

## Licence
GNU GENERAL PUBLIC LICENSE Version 3

## Auteurs
Arnaud Bey