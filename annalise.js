import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {

  let formattedData = []
  let strings = {}
  annalise.getData().then(data => {
    let originalData = JSON.parse(data)
    originalData.forEach(element => {
      let formatedElement = {}
      formatedElement.id = element.id
      element.element_texts.forEach(metadataObj => {
        let value = metadataObj.text
        let metadata = metadataObj.element.name
        let metadataSlug = annalise.slugify(metadata)
        if (formatedElement[metadataSlug]) {
          formatedElement[metadataSlug].push(value)
        } else {
          formatedElement[metadataSlug] = [value]
        }
        strings[metadataSlug] = metadata
      });
      formattedData.push(formatedElement)
    });

    const params = {
      data: formattedData,
      apiUrl: null,
      itemBaseUrl: "https://anna-lise.elan-numerique.fr/items/show/",
      filters: ["motif-theme", "annee-de-parution", "outil-procede-parametre-filmique", "type-danalyse", "type-de-texte", "revue", "source", "publisher"],
      hierarchicalOptions: ["", "annee-de-parution", "motif-theme", "outil-procede-parametre-filmique", "type-danalyse", "type-de-texte", "publisher"],
      itemProperties: ["pour-citer", "id"],
      linkProperty: "id",
      strings: strings
    }

    const e = Object.create(Explorer)
    e.init(params)
  });


});

let annalise = {
  getData: async function () {
    const options = {
      method: 'GET',
      mode: "cors",
    };
    return await fetch("./data/annalise.json", options)
      .then((response) => {
        return response.text()
      })

  },
  slugify: function (str) {
    return String(str)
      .normalize('NFKD') // split accented characters into their base characters and diacritical marks
      .replace(/[\u0300-\u036f]/g, '') // remove all the accents, which happen to be all in the \u03xx UNICODE block.
      .trim() // trim leading or trailing whitespace
      .toLowerCase() // convert to lowercase
      .replace(/[^a-z0-9 -]/g, '') // remove non-alphanumeric characters
      .replace(/\s+/g, '-') // replace spaces with hyphens
      .replace(/-+/g, '-'); // remove consecutive hyphens
  }
}