import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {
  const params = {
    apiUrl: "https://dfsm.elan-numerique.fr/api/entries-json",
    itemBaseUrl: "https://dfsm.elan-numerique.fr/entry/",
    filters: ["initial", "domain-med", "domain-mod", "grams"],
    hierarchicalOptions: ["", "initial", "domain-med", "domain-mod", "grams"],
    itemProperties: ["lemma"],
    linkProperty: "lemma",
    strings: {
      "initial": "Initiale",
      "domain-med": "Domaine médiéval",
      "domain-mod": "Domaine moderne",
      "grams": "Information de sous-catégorisation",
    }
  }

  const e = Object.create(Explorer)
  e.init(params)
});