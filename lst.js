import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {
  const params = {
    apiUrl: "https://lst.demarre-shs.fr/api/lexical-entries",
    itemBaseUrl: "https://lst.demarre-shs.fr/lexicalentry/display/",
    filters: ["cat", "lexicalType", "semanticClass", "semanticSubClass"],
    hierarchicalOptions: ["", "cat", "lexicalType", "semanticClass", "semanticSubClass"],
    itemProperties: ["acceptionUniq", "lemma", "id"],
    linkProperty: "id",
    strings: {
      "cat": "Catégorie",
      "lexicalType": "Type",
      "semanticClass": "Classe sémantique",
      "semanticSubClass": "Sous-classe sémantique",
      "A": "Adjectif",
      "ADV": "Adverbe",
      "N": "Nom",
      "V": "Verbe"
    }
  }

  const e = Object.create(Explorer)
  e.init(params)
});