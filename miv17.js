import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {

  let mivData = []
  let strings = {
    "ID": "ID",
    "author_name": "Auteur",
    "profileDesc_textclass_keywords": "Keyword",
    "hand_desc": "hand_desc",
    "editor": "Editeur",
    "dimensions_type": "dimensions_type",
    "support_material": "support_material",
    "date": "date",
    "date_type": "Type de date",
    "date_start": "Date de début",
    "date_end": "Date de fin",
    "textLang": "Langue",
    "bindingDesc": "bindingDesc"
  }
  miv17.getData().then(data => {
    mivData = JSON.parse(data)

    const params = {
      data:  JSON.parse(data),
      apiUrl: null,
      itemBaseUrl: "",
      filters: ["author_name", "profileDesc_textclass_keywords",
      "editor", "dimensions_type", "support_material", "textLang"],
      hierarchicalOptions: ["", "author_name", "profileDesc_textclass_keywords", "hand_desc", 
      "editor", "dimensions_type", "support_material", "date", "date_type", 
      "date_start", "date_end", "textLang", "bindingDesc"],
      itemProperties: ["title", "author_name", "date"],
      linkProperty: null,
      strings: strings
    }

    const e = Object.create(Explorer)
    e.init(params)
  });

});

let miv17 = {
  getData: async function () {
    const options = {
      method: 'GET',
      mode: "cors",
    };
    return await fetch("./data/miv17.json", options)
      .then((response) => {
        return response.text()
      })

  }
}