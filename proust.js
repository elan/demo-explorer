import { Explorer } from './node_modules/elan-data-explorer.js/index.js'

window.addEventListener("load", (event) => {
  const params = {
    apiUrl: "https://proust.elan-numerique.fr/api/letters-preprod",
    itemBaseUrl: "https://proust.elan-numerique.fr/letter/",
    filters: ["from", "to", "tags"],
    hierarchicalOptions: ["", "from", "to", "date", "tags"],
    itemProperties: ["from", "to", "date", "id"],
    linkProperty: "id",
    strings: {
      from: "De",
      to: "à",
      date: "Date",
      tags: "tags"
    }
  }

  const e = Object.create(Explorer)
  e.init(params)
});